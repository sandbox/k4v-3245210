This module provides a service class to generate LaTeX documents from Twig templates.

In the directory latex_templates I provide two example files.

You need this wrapper class:  
https://github.com/j-schumann/tex-wrapper

You can install it with "composer require vrok/tex-wrapper"
