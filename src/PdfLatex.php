<?php

namespace Drupal\pdflatex;

use Drupal\kli_custom\Entity\Facility;
use TexWrapper\Wrapper;

/**
 * Pdflatex service.
 *
 * @todo
 *    * Configure destination folder
 */
class PdfLatex {

  /**
   *
   */
  public function render($data, $template) {
    $template = drupal_get_path('module', 'pdflatex') . "/latex_templates/$template.tex";
    if (!$template) {
      return NULL;
    }

    // This is necessary to make twig_render_template available.
    \Drupal::service('theme.initialization')->initTheme('classy');

    $tex = twig_render_template($template, $data);

    $date = date('Y-m-d', strtotime('now'));
    $year = $data['year'];

    $facility_id = str_replace('.', '_', $data['facility']['id']);
    $facility_name = strtolower(str_replace(' ', '_', $data['facility']['name']));

    $filename = "/tmp/$date-kilaini-abrechnung-$year-$facility_id-$facility_name";

    $wrapper = new Wrapper($filename);
    $wrapper->saveTex($tex);

    $command = $wrapper->getCommand();
    $wrapper->setCommand('/usr/bin/' . $command);

    $result = $wrapper->buildPdf();
    if ($result) {
      return $wrapper->getPdfFile();
    }

    return NULL;
  }

}
